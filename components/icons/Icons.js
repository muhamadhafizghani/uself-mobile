import React from 'react';
import { Image } from 'react-native';

export function DetailsIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/details_black.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function DetailsIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/details_blue.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function TestIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/test_black.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function TestIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/test_blue.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}


export function BadgeIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/badge_black_icon.png`)}
        fadeDuration={0}
        style={{width: 23, height: 24}}
      />
  );
}

export function BadgeIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/badge_blue_icon.png`)}
        fadeDuration={0}
        style={{width: 23, height: 24}}
      />
  );
}

export function HomeIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/home_blue_icon.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function HomeIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/home_black_icon.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function CourseIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/course_black_icon.png`)}
        fadeDuration={0}
        style={{width: 23, height: 24}}
      />
  );
}

export function CourseIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/course_blue_icon.png`)}
        fadeDuration={0}
        style={{width: 23, height: 24}}
      />
  );
}

export function ProfileIconBlack() {
  return (
    <Image
        source={require(`../../assets/images/profile_black_icon.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}

export function ProfileIconBlue() {
  return (
    <Image
        source={require(`../../assets/images/profile_blue_icon.png`)}
        fadeDuration={0}
        style={{width: 24, height: 24}}
      />
  );
}